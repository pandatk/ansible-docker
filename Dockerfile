FROM alpine:3.8

ARG BUILD_DATE
ARG VERSION
ARG VCS_REF

RUN apk --no-cache add \
        ca-certificates \
        git \
        openssh-client \
        openssl \
        py-pip \
        python \
        rsync \
        sshpass

RUN apk --no-cache add --virtual \
        .build-deps \
        python-dev \
        libffi-dev \
        openssl-dev \
        build-base \
        &&\
    pip install --upgrade \
        pip \
        cffi \
        netaddr \
        &&\
    pip install \
        ansible==${VERSION} \
        &&\
    apk del \
        .build-deps \
        &&\
    rm -rf /var/cache/apk/*

RUN mkdir -p /etc/ansible \
    &&\
    echo -e "\nHost *\n\tStrictHostKeyChecking no\n\tUserKnownHostsFile=/dev/null" >> /etc/ssh/ssh_config
